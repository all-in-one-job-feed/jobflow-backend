import requests

url = "http://127.0.0.1:8000/scraper"
data = [
    {
        "category": "Backend Engineer",
        "path": "local",
        "directory_name": "backend",
        "source": "indeed"
    },
    {
        "category": "Frontend Engineer",
        "path": "local",
        "directory_name": "frontend",
        "source": "indeed"
    }
]

try:
    for d in data:
        res = requests.post(url, json=d)
    print("Populate data sucessful!")
except Exception:
    print("Error: populate data failed!")
