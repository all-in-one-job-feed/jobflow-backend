# JobFlow
A full-stack web application with the goal of consolidate all software engineering jobs in one place.

### Backend
Currently, it has APIs for authentcation through JWT, scraping Indeed, get posting from database, bookmark job posting.

### Built With
 * django
 * Django Rest framework
 * BeatifulSoup4
 * JWT
 * Sqlite3

### Installation
#### Running the fullstack locally
```sh
docker-compose up --build
```
This will setup two docker images, frontend(web), backend(api).
The frontend url is at http://127.0.0.1:5173/
The Backend URL is at http://127.0.0.1:8000/

To populate inital data in the database run:
```sh
python populate_db.py
```
finally go to the frontend url on your browser.

#### Running the backend only locally
```sh
docker build -t [tag-name]
docker run -p 8000:8000 [tag-name] .
python popluate_db.py
```

 ### API Endpoints

#### GET

Gets all the jobs from the database.
 ```yml
url:  http://127.0.0.1:8000/posts
method: get
 ```

 Gets all the category and locations from the database.
 ```yml
url:  http://127.0.0.1:8000/filter
method: get
 ```

 Gets all the jobs based on the filter criteria.
 ```yml
url:  http://127.0.0.1:8000/search?category=Backend Engineer&location=Seattle
method: get
 ```

 #### POST
 Scrapes the jobs based on the parameter and store it into database.
 ```yml
url:  http://127.0.0.1:8000/scrape
method: post
params:
   category: Backend Engineer
   path: local
   directory_name: backend
   source: indeed
 ```