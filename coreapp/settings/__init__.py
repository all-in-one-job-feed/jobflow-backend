import os
import logging
from .base import *

logger = logging.getLogger("jobflow")

# set "project_mode = 'prod'" as an environment variable
# in your OS (on which your website is hosted)
mode = os.getenv("BUILD_MODE", "dev")

logger.info(f"Application enviornment mode: {mode}")
print("============================")
print("env mode " + mode)
print("============================")

if mode == "prod":
    from .prod import *
elif mode == "stage":
    from .stage import *
else:
    from .dev import *
