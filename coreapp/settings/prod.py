DEBUG = False
CSRF_TRUSTED_ORIGINS = ['https://jobflow.wuyuanchen.com']
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_ALLOW_ALL = False
CORS_ALLOWED_ORIGINS = [
    'http://localhost:5173',
    'http://127.0.0.1:5173',
    "http://192.9.233.124:5173",
    "https://jobflow.wuyuanchen.com",
]
ALLOWED_HOSTS = ['localhost', '127.0.0.1',
                 "192.9.233.124", "jobflow.wuyuanchen.com"]

CELERY_BROKER_URL = "amqp://guest:guest@rabbitmq"
