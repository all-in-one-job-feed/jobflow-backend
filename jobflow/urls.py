from django.urls import path
from jobflow import views

urlpatterns = [
    path('posts/', views.PostList.as_view(), name="post_view"),
    path('filter/', views.FilterList.as_view(), name="filter_view"),
    path('search/', views.SearchList.as_view(), name="search_view"),
    path('bookmark/', views.BookmarkList.as_view(), name="bookmark_view"),
    path('bookmark/<int:post_id>/', views.BookmarkList.as_view(),
         name="bookmark_delete_view"),
]
