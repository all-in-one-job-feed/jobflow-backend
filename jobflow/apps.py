from django.apps import AppConfig


class JobflowConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'jobflow'
