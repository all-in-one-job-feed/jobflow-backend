import logging
from django.contrib.auth import get_user_model
from rest_framework import serializers
from jobflow.models import (
    PostSummary,
    Location,
    Company,
    Category,
    Bookmark,
)


logger = logging.getLogger("jobflow")


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ["name"]

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        return representation["name"]


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ["name"]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["name"]

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        return representation["name"]


class PostSummarySerializer(serializers.ModelSerializer):
    company = serializers.CharField(source='company.name')
    locations = LocationSerializer(many=True)
    categories = CategorySerializer(many=True)
    is_bookmarked = serializers.SerializerMethodField("get_is_bookmarked")

    class Meta:
        model = PostSummary
        fields = ["id", "title", "company", "categories", "locations",
                  "detail", "job_link", "source", "external_id", "is_bookmarked"]

    def get_is_bookmarked(self, obj):
        # Get the user from the request
        request = self.context.get('request', None)
        if not request:
            return False

        User = get_user_model()
        # Check if the post is bookmarked for the use
        if User.objects.filter(username=request.user).exists():
            bookmarked_posts = Bookmark.objects.filter(
                user=request.user, posts=obj)

            # Check if the post is in the set of bookmarked posts
            return bookmarked_posts.exists()

        # no bookmarks for unregistered users
        return False

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['detail'] = self.to_list(instance.detail)
        representation['locations'] = self.to_list_from_dict(
            instance.locations.all())
        representation['categories'] = self.to_list_from_dict(
            instance.categories.all())
        return representation

    def to_list(self, val: str) -> list:
        return [item.strip() for item in val.split(
            '::')] if val else []

    def to_list_from_dict(self, item: list[dict]) -> list[str]:
        return [x.name for x in item]

    def create(self, validated_data):
        company_name = validated_data.get("company")
        locations = validated_data.pop("locations", [])
        categories = validated_data.pop("categories", [])

        # Ensure the object exists or create it
        company, _ = Company.objects.get_or_create(**company_name)
        loc_objs = [Location.objects.get_or_create(
            **loc)[0] for loc in locations]
        cat_objs = [Category.objects.get_or_create(
            **cat)[0] for cat in categories]

        # Update the post_data dictionary with the object
        validated_data['company'] = company

        post_summary = PostSummary.objects.create(**validated_data)
        post_summary.locations.set(loc_objs)
        post_summary.categories.set(cat_objs)
        return post_summary


class BookmarkSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.username')
    added_posts = serializers.SerializerMethodField("get_added_posts")

    class Meta:
        model = Bookmark
        fields = ['id', 'user', 'added_posts', 'posts']

    def get_added_posts(self, obj):
        try:
            # this will contain value only when creat or update bookmark
            return self.added_posts
        except AttributeError:
            return []

    def create(self, validated_data):
        logger.info(f"deserializing Bookmark: {validated_data}")
        # Extract the 'user' field from the validated data
        username = validated_data.pop('user')["username"]

        try:
            # Get the user based on the extracted username
            User = get_user_model()
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            logger.info(
                f"User not found during deserializing process: {username}")
            # Handle the case where the user is not found
            raise serializers.ValidationError("User not found.")

        bookmark, is_created = Bookmark.objects.get_or_create(user=user)
        self.added_posts = []
        for post in validated_data.get("posts", []):
            bookmark.posts.add(post)
            self.added_posts.append(post.id)

        logger.info("deserializing Bookmark successful.")
        return bookmark
