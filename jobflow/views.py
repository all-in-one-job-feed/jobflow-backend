import logging
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.db.models.query import QuerySet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny
from jobflow.models import (
    PostSummary,
    Location,
    Category,
    Bookmark,
)
from jobflow.serializers import (
    PostSummarySerializer,
    CategorySerializer,
    LocationSerializer,
    BookmarkSerializer,
)
# Create your views here.

logger = logging.getLogger("jobflow")


class PostList(APIView):
    permission_classes = [AllowAny]

    def get(self, request, format=None):
        page = int(request.query_params.get("page", 1))
        start_index = (page - 1) * settings.JOBS_PER_PAGE
        end_index = start_index + settings.JOBS_PER_PAGE
        posts = PostSummary.objects.all()
        # adding context so the serializer can have access to the request
        s = PostSummarySerializer(
            posts[start_index:end_index],
            many=True,
            context={"request": request})
        resp = {
            "totalPost": posts.count(),
            "page": 1,
            "perPage": len(s.data),
            "jobPosts": s.data,
        }
        return Response(resp, status=status.HTTP_200_OK)


class FilterList(APIView):
    permission_classes = [AllowAny]

    def get(self, request, format=None):
        origin_url = request.META.get('HTTP_REFERER', 'Unknown')
        logger.info(f"fetch /filter from {origin_url}.")
        try:
            loc_objs = Location.objects.all()
            loc = LocationSerializer(loc_objs, many=True)

            cat_objs = Category.objects.all()
            cat = CategorySerializer(
                cat_objs, many=True)
            res = {
                "locations": sorted(loc.data),
                "categories": sorted(cat.data)
            }

            return Response(res)
        except Exception as e:
            logger.error(f"Fail to fetch /filter due to error: {e}")
            return Response({"error": "Internal Server Error"},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR
                            )


class SearchList(APIView):
    permission_classes = [AllowAny]

    def get(self, request, format=None):
        category = request.query_params.get("category")
        location = request.query_params.get("location")
        page = int(request.query_params.get("page", 1))
        start_index = (page - 1) * settings.JOBS_PER_PAGE
        end_index = start_index + settings.JOBS_PER_PAGE
        logger.info(
            f"category: {category}, location: {location}, page: {page}")

        queryset = None
        s_data = []
        count = 0

        if category:
            queryset: QuerySet = self.filter_by_category(category)
        if location:
            queryset: QuerySet = self.filter_by_location(location, queryset)
        if queryset:
            count = queryset.count()
            s_data = PostSummarySerializer(
                queryset[start_index:end_index],
                many=True,
                context={"request": request}).data
        else:
            posts = PostSummary.objects.all()
            count = posts.count()
            # adding context so the serializer can have access to the request
            s_data = PostSummarySerializer(
                posts[start_index:end_index],
                many=True,
                context={"request": request}).data

        resp = {
            "totalPost": count,
            "page": page,
            "perPage": len(s_data),
            "jobPosts": s_data,
        }
        return Response(resp, status=status.HTTP_200_OK)

    def filter_by_category(self, category: str) -> QuerySet:
        cat_id: int = get_object_or_404(Category, name=category).id
        return PostSummary.objects.filter(categories__id=cat_id)

    def filter_by_location(self,
                           location: str,
                           queryset: QuerySet = None
                           ) -> QuerySet:
        loc_id: int = get_object_or_404(Location, name=location).id
        if queryset:
            queryset = queryset.filter(locations__id=loc_id)
        else:
            queryset = PostSummary.objects.filter(locations__id=loc_id)
        return queryset


class BookmarkList(APIView):
    def get(self, request):
        page = int(request.query_params.get("page", 1))
        start_index = (page - 1) * settings.JOBS_PER_PAGE
        end_index = start_index + settings.JOBS_PER_PAGE
        bm = get_object_or_404(Bookmark, user=request.user)
        posts = bm.posts.all()
        s = PostSummarySerializer(
            posts[start_index: end_index],
            many=True,
            context={"request": request}
        )

        resp = {
            "totalPost": posts.count(),
            "page": page,
            "perPage": len(s.data),
            "jobPosts": s.data,
        }
        return Response(resp)

    def post(self, request, format=None):
        request.data["user"] = request.user.username
        s = BookmarkSerializer(data=request.data)
        if s.is_valid():
            s.save()
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(s.data,
                        status=status.HTTP_201_CREATED)

    def delete(self, request, post_id):
        logger.info(f"deleting post id {post_id} from bookmark.")
        # Delete the post from the bookmark based on the user and post_id
        bookmark_instance = get_object_or_404(Bookmark, user=request.user)

        try:
            # Use 'remove()' to remove the post from the 'posts' field
            post_to_remove = bookmark_instance.posts.get(id=post_id)
            bookmark_instance.posts.remove(post_to_remove)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Bookmark.posts.model.DoesNotExist:
            return Response({"detail": "Post not found in the bookmark."},
                            status=status.HTTP_404_NOT_FOUND)
