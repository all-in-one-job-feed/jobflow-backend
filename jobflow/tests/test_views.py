from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from jobflow.models import (
    PostSummary,
    Location,
    Category,
    Company,
)
from jobflow.serializers import (
    PostSummarySerializer,
    LocationSerializer,
    CategorySerializer,
)


class PostListTestCase(TestCase):
    def setUp(self):
        """
        setting up an model objects for testing, this will store in
        a test database, and will be destoryed once the test is ran. 
        """
        self.client = APIClient()
        self.seattle = Location.objects.create(name="Seattle")
        self.new_york = Location.objects.create(name="New York")
        self.backend = Category.objects.create(name="Backend Engineer")
        self.frontend = Category.objects.create(name="Frontend Engineer")
        self.nasa = Company.objects.create(name="NASA")
        self.nasa_backend_post = PostSummary.objects.create(
            title="Senior API Engineer",
            company=self.nasa,
            detail="100 an hour, remote, 4 day week"
        )

        self.nasa_backend_post.locations.add(self.seattle)
        self.nasa_backend_post.categories.add(self.backend)

    def test_PostList_GET(self):
        # get the url by using reverse
        url = reverse("post_view")

        # fetch a GET request
        resp = self.client.get(url)
        self.assertEquals(resp.status_code, 200)
        expected = []
        expected.append(PostSummarySerializer(
            self.nasa_backend_post).data)
        # assert the response data is the same as expected
        self.assertEqual(resp.json()["jobPosts"], expected)


class FilterListTestCase(TestCase):
    def setUp(self):
        """
        setting up an model objects for testing, this will store in
        a test database, and will be destoryed once the test is ran. 
        """
        self.client = APIClient()
        self.seattle = Location.objects.create(name="Seattle")
        self.new_york = Location.objects.create(name="New York")
        self.backend = Category.objects.create(name="Backend Engineer")
        self.frontend = Category.objects.create(name="Frontend Engineer")
        self.nasa = Company.objects.create(name="NASA")
        self.nasa_backend_post = PostSummary.objects.create(
            title="Senior API Engineer",
            company=self.nasa,
            detail="100 an hour, remote, 4 day week"
        )

        self.nasa_backend_post.locations.add(self.seattle)
        self.nasa_backend_post.categories.add(self.backend)

    def test_FilterList_GET(self):
        url = reverse("filter_view")

        resp = self.client.get(url)
        self.assertEquals(resp.status_code, 200)
        expected = {
            "locations": ["Seattle", "New York"],
            "categories": ["Backend Engineer", "Frontend Engineer"]
        }
        self.assertEqual(resp.json(), expected)


class SearchListTestCase(TestCase):
    def setUp(self):
        """
        setting up an model objects for testing, this will store in
        a test database, and will be destoryed once the test is ran. 
        """
        self.client = APIClient()
        self.seattle = Location.objects.create(name="Seattle")
        self.new_york = Location.objects.create(name="New York")
        self.backend = Category.objects.create(name="Backend Engineer")
        self.frontend = Category.objects.create(name="Frontend Engineer")
        self.nasa = Company.objects.create(name="NASA")
        self.netflix = Company.objects.create(name="Netflix")
        self.nasa_backend_post = PostSummary.objects.create(
            title="Senior API Engineer",
            company=self.nasa,
            detail="100 an hour, remote, 4 day week",
            job_link="www.nasa.com"
        )
        self.netflix_backend_post1 = PostSummary.objects.create(
            title="Software Engineer, Backend",
            company=self.netflix,
            detail="$150k per year, in office, hybird",
            job_link="www.netflix.com/job1"
        )
        self.netflix_backend_post2 = PostSummary.objects.create(
            title="Software Engineer, Python",
            company=self.netflix,
            detail="$150k per year, in office, hybird",
            job_link="www.netflix.com/job2"
        )

        self.nasa_backend_post.locations.add(self.seattle)
        self.nasa_backend_post.categories.add(self.backend)

        self.netflix_backend_post1.locations.add(self.seattle)
        self.netflix_backend_post1.categories.add(self.backend)

        self.netflix_backend_post2.locations.add(self.new_york)
        self.netflix_backend_post2.categories.add(self.backend)

    def test_GET_filter_cat_and_loc(self):
        query_params = "?category=Backend Engineer&location=Seattle"
        url = reverse("search_view") + query_params
        resp = self.client.get(url)
        self.assertEquals(resp.status_code, 200)

    def test_GET_filter_cat(self):
        query_params = "?category=Backend Engineer"
        url = reverse("search_view") + query_params
        resp = self.client.get(url)
        self.assertEquals(resp.status_code, 200)

    def test_GET_filter_loc(self):
        query_params = "?location=New York"
        url = reverse("search_view") + query_params
        resp = self.client.get(url)
        self.assertEquals(resp.status_code, 200)
