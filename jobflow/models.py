from django.db import models
from user.models import User


class Location(models.Model):
    name = models.CharField(max_length=100, blank=True, default="")


class Company(models.Model):
    name = models.CharField(max_length=100, blank=True, default="")


class Category(models.Model):
    name = models.CharField(max_length=100, blank=True, default="")


# Create your models here.
class PostSummary(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default="")
    categories = models.ManyToManyField(Category)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    locations = models.ManyToManyField(Location)
    detail = models.CharField(max_length=200, blank=True, default="")
    job_link = models.TextField()
    source = models.CharField(max_length=200, blank=True, default="")
    external_id = models.CharField(max_length=200, blank=True, default="")

    class Meta:
        ordering = ["created"]
        unique_together = ["source", "external_id"]


class Bookmark(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    posts = models.ManyToManyField(PostSummary)
    created_at = models.DateTimeField(auto_now_add=True)
