import logging
from rest_framework import serializers
from user.models import User

logger = logging.getLogger("jobflow")


class UserSerializer(serializers.ModelSerializer):

    # write_only=True will ensure that the field may
    # be used when updating or creating an instance,
    # but is not included when serializing the representation.
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ["id", "username", "password", "email",
                  "first_name", "last_name", "is_active"]

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        password = validated_data.pop('password', None)
        if password:
            user.set_password(password)
            user.save()

        return user
