from django.urls import path
from user.views import (
    profile,
    login_view,
    register_view,
    logout_view,
    check_auth_view,
)

urlpatterns = [
    path('profile/', profile, name='profile'),
    path('login/', login_view, name='login'),
    path('register/', register_view, name='register'),
    path('logout/', logout_view, name='logout'),
    path("check-auth/", check_auth_view, name="check-auth")
]
