import logging
from django.http import HttpResponse
from django.contrib.auth import get_user_model
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import status
from rest_framework.exceptions import (
    AuthenticationFailed,
    ValidationError,
)
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from user.serializers import UserSerializer
from user.utils import (
    generate_access_token,
    generate_refresh_token
)


logger = logging.getLogger("jobflow")


@api_view(['GET'])
def profile(request):
    logger.info("requesting profile")
    user = request.user
    serialized_user = UserSerializer(user).data
    return Response({'user': serialized_user}, status=status.HTTP_200_OK)


@api_view(['GET'])
def check_auth_view(request):
    logger.info("requesting is_logged_in_view")
    user = request.user
    serialized_user = UserSerializer(user).data
    return Response({'user': serialized_user}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([AllowAny])
@ensure_csrf_cookie
def login_view(request):
    User = get_user_model()
    username = request.data.get("username")
    password = request.data.get("password")
    logger.info(f"submitting login info for user {username}")

    response = Response()

    if (not username) or (not password):
        logger.error(
            "Failed submitting login info. Username and password required")
        raise AuthenticationFailed(
            "username and password required"
        )

    user = User.objects.filter(username=username).first()
    if not user:
        logger.error("User not found.")
        raise AuthenticationFailed('User not found')
    if not user.check_password(password):
        logger.error("Incorrect password.")
        raise AuthenticationFailed('Incorrect password')

    s_user = UserSerializer(user).data
    access_token = generate_access_token(user)
    refresh_token = generate_refresh_token(user)
    response.set_cookie(key='accessToken', value=access_token, httponly=True)
    response.set_cookie(key='refreshToken', value=refresh_token, httponly=True)
    response.data = {
        'user': s_user,
    }

    logger.info("submitting login info successfully!")
    return response


@api_view(['POST'])
@permission_classes([AllowAny])
def register_view(request):
    logger.info("submitting registration information.")
    try:
        s = UserSerializer(data=request.data)
        s.is_valid(raise_exception=True)
        s.save()
    except ValidationError as e:
        logger.error(f"Failed to register: {e}, {s.errors}")
        return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
    logger.info("register successfully!")
    return Response(s.data, status=status.HTTP_201_CREATED)


@api_view(['GET'])
def logout_view(request):
    logger.info("logging out...")

    response = HttpResponse({"detail": "Cookies Deleted"})
    response.delete_cookie('accessToken', path='/')
    response.delete_cookie('refreshToken', path='/')
    logger.info("logout successfully.")
    return response
