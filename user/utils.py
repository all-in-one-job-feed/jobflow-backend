import os
import logging
import datetime as dt
from user.models import User
from django.conf import settings
from django.middleware.csrf import CsrfViewMiddleware
from django.contrib.auth import get_user_model
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import (
    AuthenticationFailed,
    PermissionDenied
)
import jwt

logger = logging.getLogger("jobflow")


def generate_access_token(user: User):
    payload = {
        "user_id": user.id,
        "exp": dt.datetime.utcnow() + dt.timedelta(days=1, minutes=5),
        "iat": dt.datetime.utcnow()
    }

    token = jwt.encode(payload, settings.SECRET_KEY,
                       algorithm="HS256")
    return token


def generate_refresh_token(user: User):
    payload = {
        'user_id': user.id,
        'exp': dt.datetime.utcnow() + dt.timedelta(days=7),
        'iat': dt.datetime.utcnow()
    }
    refresh_token = jwt.encode(
        payload, settings.REFRESH_TOKEN_SECRET, algorithm='HS256')

    return refresh_token


class JWTAuthentication(BaseAuthentication):
    def __get_access_token(self, request):
        access_token = request.COOKIES.get('accessToken')
        if access_token:
            return access_token

        auth_header = request.headers.get('Authorization')
        if auth_header:
            return auth_header.split(' ')[1]
        return None

    def authenticate(self, request):
        access_token = self.__get_access_token(request)
        if not access_token:
            return
        try:
            payload = jwt.decode(
                access_token, settings.SECRET_KEY, algorithms=['HS256']
            )
        except jwt.ExpiredSignatureError:
            logger.error("jwt expired.")
            raise AuthenticationFailed("user not found.")
        except IndexError:
            logger.error("Token prefix missing.")
            raise AuthenticationFailed("Token prefix missing.")

        User = get_user_model()
        user = User.objects.filter(id=payload["user_id"]).first()
        if not user:
            logger.error("user not found.")
            raise AuthenticationFailed('User not found.')

        if not user.is_active:
            logger.error("user is inactive.")
            raise AuthenticationFailed('User is inactive.')
        self.enforce_csrf(request)
        return (user, None)

    def enforce_csrf(self, request):
        """
        Enforce CSRF validation
        """
        def dummy_get_response(request):
            return None
        check = CSRFCheck(dummy_get_response)
        # populates request.META['CSRF_COOKIE'],
        # which is used in process_view()
        check.process_request(request)
        reason = check.process_view(request, None, (), {})
        logger.error(reason)
        if reason:
            # CSRF failed, bail with explicit error message
            raise PermissionDenied('CSRF Failed: %s.' % reason)


class CSRFCheck(CsrfViewMiddleware):
    def _reject(self, request, reason):
        # Return the failure reason instead of an HttpResponse
        return reason
