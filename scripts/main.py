'''
Usage: python manage.py shell < script/main.py
'''
from jobflow.serializers import PostSummarySerializer, LocationSerializer, CompanySerializer, CategorySerializer, BookmarkSerializer  # nopep8
from django.contrib.auth import get_user_model  # nopep8
from rest_framework.parsers import JSONParser  # nopep8
from rest_framework.renderers import JSONRenderer  # nopep8
from jobflow.models import PostSummary, Location, Company, Category, Bookmark  # nopep8


def add_bookmark(data):
    s = BookmarkSerializer(data=data)
    s.is_valid(raise_exception=True)
    s.save()
    s.data


def main():
    User = get_user_model()

    admin = User.objects.filter(username="admin").first()
    test9 = User.objects.filter(username="test9").first()

    posts = PostSummary.objects.all()

    p8 = posts[8]

    data = {"user": admin.username, "posts": [p8.id]}
    add_bookmark(data)

    data = {"user": test9.username, "posts": [p8.id]}
    add_bookmark(data)


main()
