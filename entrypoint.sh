#!/bin/sh
python manage.py makemigrations jobflow --no-input
python manage.py migrate --no-input
python manage.py collectstatic --no-input

celery -A coreapp worker --loglevel=info > celery.log 2>&1 &
gunicorn coreapp.wsgi:application --bind 0.0.0.0:8000 --timeout 600