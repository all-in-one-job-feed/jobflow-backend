import logging
import collections
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from scraper.tasks import (
    get_all_post_detail,
)
from jobflow.serializers import PostSummarySerializer

logger = logging.getLogger("jobflow")


class ScraperList(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        title = request.data.get("title")
        location = request.data.get("location")
        source = request.data.get("source")
        # indeed page are increment of 10
        page_start = request.data.get("page_start", 0)
        page_end = request.data.get("page_end", 100)

        url = "https://www.indeed.com/jobs"
        url += f"?q={title}&l={location}&radius=35&filter=1&sort=date"
        url += "&start=%s"

        task_id = get_all_post_detail.delay(
            source, title, page_start, page_end, url)

        return Response({"Task Id": str(task_id)},
                        status=status.HTTP_201_CREATED)


class ScraperLocalList(APIView):
    permission_classes = [AllowAny]

    def get(self, request, format=None):
        return Response({"hello": "world"})

    def post(self, request, format=None):
        category = request.data.get("category")
        source = request.data.get("source")

        job_id = get_all_post_detail.delay(source, category)
        # job_id = get_all_post_detail(source, category)

        return Response({"job_id": str(job_id)},
                        status=status.HTTP_201_CREATED)


class StoreData(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format=None):
        posts = request.data.get("posts")
        res = collections.defaultdict(list)
        res["SUCCESS"] = {}
        res["FAILED"] = {}
        res["ERR_MSG"] = {}

        for i, post in enumerate(posts):
            job_link = post.get("job_link")
            s = PostSummarySerializer(data=post)
            if s.is_valid():
                s.save()
                res["SUCCESS"][i] = job_link
            else:
                res["FAILED"][i] = job_link
                for label, msg in s.errors.items():
                    res["ERR_MSG"][i] = msg

        return Response(res,
                        status=status.HTTP_201_CREATED)
