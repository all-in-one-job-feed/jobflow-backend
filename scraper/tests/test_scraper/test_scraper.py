from bs4 import BeautifulSoup
from django.test import TestCase
from unittest.mock import patch
from django.conf import settings
from scraper.utils.scraper import JobScraper, IndeedScraper, Indeed, get_all_post_detail
from scraper.utils.html_reader import HtmlReader, LocalHtmlReader

# Create your tests here.


class IndeedScraperTestCase(TestCase):
    def setUp(self):
        path = settings.PROJECT_ROOT + "scraper/tests/data/test_scraper"
        html_reader: HtmlReader = LocalHtmlReader(path)
        self.scraper = IndeedScraper(html_reader)

    def test_get_job_posts(self):
        actual = self.scraper.get_job_posts()
        actual_len = len(actual)
        self.assertEqual(actual_len, 45)


class IndeedTestCase(TestCase):
    def setUp(self):
        path = settings.PROJECT_ROOT + "scraper/tests/data/test_scraper"
        html_reader: HtmlReader = LocalHtmlReader(path)
        scraper = IndeedScraper(html_reader)
        posts = scraper.get_job_posts()
        self.indeedScraper = Indeed(posts[0], "Backend Engineer")

    def test_get_title(self):
        actual = self.indeedScraper.get_title()
        expect = "Software Engineer - Content Lifecycle Engineering (L5, Backend)"
        self.assertEqual(actual, expect)

    def test_get_company(self):
        actual = self.indeedScraper.get_company()
        expect = "Netflix"
        self.assertEqual(actual, expect)

    def test_get_locations(self):
        actual = self.indeedScraper.get_locations()
        expect = [{"name": "Los Gatos"}]
        self.assertEqual(actual, expect)

    def test_get_detail(self):
        actual = self.indeedScraper.get_detail().split(",")
        expect = ['On call', 'Estimated $109K - $139K a year']
        self.assertTrue(any(item in expect for item in actual))

    def test_get_categories(self):
        actual = self.indeedScraper.get_categories()
        expect = [{"name": "Backend Engineer"}]
        self.assertEqual(actual, expect)

    def test_get_job_link(self):
        actual = self.indeedScraper.get_job_link()
        expect = "https://www.indeed.com/viewjob?jk=7918b16ba0a2c3b5&" + \
            "tk=1hggs2b7oh5ho801"
        self.assertEqual(actual, expect)

    def test_get_source(self):
        actual = self.indeedScraper.get_source()
        expect = "Indeed"
        self.assertEqual(actual, expect)


class ScraperTestCase(TestCase):
    def test_get_all_post_detail(self):
        path = settings.PROJECT_ROOT + "scraper/tests/data/test_scraper"
        html_reader: HtmlReader = LocalHtmlReader(path)
        scraper: JobScraper = IndeedScraper(html_reader)

        queue = get_all_post_detail(scraper, html_reader, "Backend Engineer")
        self.assertEqual(len(queue), 45)
