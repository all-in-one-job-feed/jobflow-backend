import os
import logging
import requests
import locationtagger
from abc import ABC, abstractmethod
from bs4 import BeautifulSoup
from bs4.element import ResultSet
from celery import Celery
from celery.signals import task_success
from scraper.utils.html_reader import HtmlReader, SeleniumHtmlReader, \
    LocalHtmlReader


ROOT_PATH = os.path.abspath(os.path.dirname(__name__))
logger = logging.getLogger("jobflow")

rabbit_host = "localhost"
build_mode = os.getenv("BUILD_MODE", "dev")
if build_mode == "prod" or build_mode == "stage":
    rabbit_host = "rabbitmq"

rabbit_broker = f'amqp://guest:guest@{rabbit_host}'

celery = Celery("scraper_task", broker=rabbit_broker,
                backend=f"db+sqlite:///{ROOT_PATH}/database/rabbitmq.sqlite3")
# # Enable signal dispatching
# celery.conf.send_task_sent_event = True
logger.info(f"Connected to rabbit broker: {rabbit_broker}")


class JobInfo(ABC):
    @abstractmethod
    def get_title():
        pass

    @abstractmethod
    def get_company():
        pass

    @abstractmethod
    def get_locations():
        pass

    @abstractmethod
    def get_detail():
        pass

    @abstractmethod
    def get_categories():
        pass

    @abstractmethod
    def category():
        pass

    @abstractmethod
    def get_job_link():
        pass

    @abstractmethod
    def get_source():
        pass


class JobScraper(ABC):
    def get_job_posts():
        """
        get the descriptions of each job posting.
        """
        pass


class IndeedScraper(JobScraper):
    def __init__(self, html_reader: HtmlReader):
        self._html_reader = html_reader

    @property
    def html_reader(self):
        return self._html_reader

    @property
    def source(self):
        return self._source

    @property
    def scraper(self):
        return self._scraper

    @property
    def title(self):
        return self._title

    @property
    def location(self):
        return self._location

    def _get_posts(self, soup: BeautifulSoup) -> list[ResultSet]:
        posts = []
        try:
            posts = soup.find_all(
                class_='job_seen_beacon'
            )
        except AttributeError:
            print("can't find posts within HTML.")

        return posts

    def get_job_posts(self, category: str) -> list[JobInfo]:
        posts = []
        for path in self.html_reader.get_all_page_path():
            try:
                logger.info(f"scraping {path}")
                html_txt = self.html_reader.get_html_page(path)
                soup = BeautifulSoup(html_txt, "lxml")
                new_posts = self._get_posts(soup)
                if new_posts:
                    for p in new_posts:
                        posts.append(Indeed(p, category))
            except Exception as err:
                logger.error(f"html_text: {html_txt}")
                logger.error(err)
        self.html_reader.quit()
        return posts


class Indeed(JobInfo):
    def __init__(self, job_post: ResultSet, category: str):
        self._job_post = job_post
        self._category = category
        self._source = "Indeed"

    @property
    def job_post(self):
        return self._job_post

    @property
    def category(self):
        return self._category

    @property
    def source(self):
        return self._source

    def get_title(self) -> str:
        title = self.job_post.find(
            "a",
            class_="jcs-JobTitle"
        ).text
        return title.strip()

    def get_company(self) -> str:
        name = self.job_post.find(
            "span", {"data-testid": "company-name"}).text

        return name.strip()

    def get_locations(self) -> str:
        outter_div = self.job_post.find(
            class_="company_location"
        )

        # Find the most inner <div> element within the outer <div>
        location_div = outter_div.find('div', {'data-testid': 'text-location'})

        # Extract the text content of the location <div>
        location = location_div.text.strip()
        entities = locationtagger.find_locations(text=location)
        logger.info(
            f"Original string: {location}, country: {entities.countries}" +
            f" states: {entities.regions}, city: {entities.cities}")
        try:
            city = entities.cities[0]
        except Exception:
            city = location
        return [{"name": city}]

    def get_detail(self) -> str:
        detail = ""
        heading = self.job_post.find(
            "div",
            class_="heading6"
        )

        innermost_texts = set([self.__get_innermost_text(
            div) for div in heading.find_all('div')])

        detail = list(innermost_texts)

        return ",".join(detail)

    def get_categories(self):
        return [{"name": self.category}]

    def get_job_link(self):
        link = ""
        a = self.job_post.find(class_='jobTitle').find("a")
        href = a.get('href')
        link = "https://www.indeed.com/%s" % href

        return link

    def get_job_id(self):
        job_id = ""
        a = self.job_post.find(class_='jobTitle').find("a")
        job_id = a.get("id")

        return job_id

    def get_source(self):
        return self.source

    def __get_innermost_text(self, element):
        if element and hasattr(element, 'contents') and len(element.contents) > 0:
            innermost_texts = [self.__get_innermost_text(
                child) for child in element.contents]
            return ' '.join(innermost_texts).strip()
        elif element and hasattr(element, 'text'):
            return element.text.strip()
        else:
            return ""


@celery.task
def get_all_post_detail(source: str,
                        category: str, url=None):

    if url:
        html_reader: HtmlReader = SeleniumHtmlReader(url)
    else:
        dir_name = category.lower().strip().replace(" ", "_")
        path = f"{ROOT_PATH}/scraper/tests/data/jobs/{dir_name}"
        html_reader: HtmlReader = LocalHtmlReader(path)

    if source.lower() == "indeed":
        scraper: JobScraper = IndeedScraper(html_reader)
    job_posts: list[JobInfo] = scraper.get_job_posts(category)
    post_queue = []
    for post in job_posts:
        post_details = {}
        try:
            post_details["title"] = post.get_title()
            post_details["company"] = post.get_company()
            post_details["locations"] = post.get_locations()
            post_details["detail"] = post.get_detail()
            post_details["categories"] = post.get_categories()
            post_details["job_link"] = post.get_job_link()
            post_details["source"] = post.get_source()
            post_details["external_id"] = post.get_job_id()

            post_queue.append(post_details)
        except Exception:
            logger.error("can't scrape the following job: {}".format(post))
            logger.error("=========== Encounter error above ===============")
    return post_queue


@task_success.connect(sender=get_all_post_detail, weak=False)
def store_to_db(sender=None, **kwargs):
    mode = os.getenv("BUILD_MODE", "dev")
    url = "http://127.0.0.1:8000/api/scraper/save"
    if mode == "prod":
        url = "http://jobflow.wuyuanchen.com/api/scraper/save"
    posts = kwargs.get("result")
    print("++++++++++++++++++++++")
    print(f"request url is: {url}")
    print(f"first post: {posts[:1]}")
    print("++++++++++++++++++++++")
    print("this is store_to_db")
    resp = requests.post(url, json={"posts": posts})
    print(f"status code is: {resp.status_code}")
