import os
import logging
import requests
import random
from abc import ABC, abstractmethod
from time import sleep
from random import randint
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager


logger = logging.getLogger("jobflow")


class HtmlReader(ABC):

    @property
    @abstractmethod
    def path(self):
        pass

    @abstractmethod
    def get_all_page_path(self):
        """
        this could be urls or a file paths
        """
        pass

    @abstractmethod
    def get_html_page(self):
        """
        this could be read from the web or locally  
        """
        pass


class LocalHtmlReader(HtmlReader):
    """
    This class read HTML files locally
    """

    def __init__(self, path):
        self._path = path

    @property
    def path(self):
        return self._path

    def get_all_page_path(self, page_start=0, page_end=30) -> list[str]:
        files: list[str] = self.__get_files(self._path)
        logger.info(f"getting the following local files to scrape {files}")
        return sorted(files)

    def get_html_page(self, path):
        html_txt = ""
        with open(path, "r") as f:
            html_txt = f.read()

        return html_txt

    def __get_files(self, d):
        logger.info(f"directory: {d}")
        files = [os.path.join(d, f) for f in os.listdir(
            d) if os.path.isfile(os.path.join(d, f))]
        logger.info(f"files: {files}")
        return files

    def quit(self):
        pass


class RequestsHtmlReader(HtmlReader):
    """
    This class uses requests module to read
    HTML
    """

    def __init__(self, path):
        self._path = path

    @property
    def path(self):
        return self._path

    def get_all_page_path(self, page_start=0, page_end=30):
        page_urls = []
        for i in range(0, 100, 10):
            page_urls.append(self._path % str(i))

        return page_urls

    def get_html_page(self, url):
        return requests.get(url).text


class SeleniumHtmlReader(HtmlReader):
    """
    This class uses selenium to get HTML
    param path eaxmple:
        'https://www.indeed.com/jobs?q={}&l={}&radius=35&filter=1&sort=date&start={}'.format("software+engineer", "san+jose", 1)
    """

    def __init__(self, path):
        self._path = path
        self._user_agents = [
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 OPR/43.0.2442.991"
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36"
        ]
        self._init_driver()

    @property
    def path(self):
        self._path

    def _init_driver(self):
        options = Options()
        options.add_experimental_option("detach", True)
        options.add_argument("--headless")  # Run Chrome headless
        options.add_argument("disable-infobars")  # disabling infobars
        options.add_argument('--no-sandbox')
        options.add_argument("--disable-setuid-sandbox")
        options.add_argument("--disable-extensions")  # disabling extensions
        # overcome limited resource problems
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument('--remote-debugging-pipe')
        user_agent = random.choice(self._user_agents)
        options.add_argument(f'user-agent={user_agent}')
        self.driver = webdriver.Chrome(service=Service(ChromeDriverManager()
                                                       .install()),
                                       options=options)

    def get_all_page_path(self, page_start=0, page_end=30):
        page_urls = []
        for i in range(page_start, page_end, 10):
            page_urls.append(self._path % str(i))

        return page_urls

    def get_html_page(self, url):
        try:
            sleep(randint(2, 6))
            self.driver.get(url)
            return self.driver.page_source
        except Exception:
            pass

    def quit(self):
        self.driver.quit()
