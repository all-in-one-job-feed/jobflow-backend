import random
from time import sleep
from random import randint
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup


def main():
    options = Options()
    options.add_experimental_option("detach", True)
    options.add_argument("headless")  # Run Chrome headless
    # reference on how to make chrome headless undetectable
    # https://intoli.com/blog/making-chrome-headless-undetectable/
    user_agents = [
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 OPR/43.0.2442.991"
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36"
    ]
    user_agent = random.choice(user_agents)
    options.add_argument(f'user-agent={user_agent}')
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()),
                              options=options)

    url = 'https://www.indeed.com/jobs?q={}&l={}&radius=35&filter=1&sort=date&start={}'
    driver.get(url.format("backend+engineer", "San+Jose+CA", 0))
    # driver.maximize_window()

    sleep(randint(2, 6))

    # job_count = driver.find_element(
    #     By.CLASS_NAME, 'jobsearch-JobCountAndSortPane-jobCount').text
    # # driver.quit()  # Closing the browser we opened

    # total_pages = int(job_count.split(' ')[0])//15
    # print(total_pages)

    html_srcs = []
    for i in range(1):
        try:
            sleep(randint(2, 6))
            driver.get(url.format("backend+engineer", "San+Jose+CA", i*10))
            html_srcs.append(driver.page_source)
        except Exception:
            pass

    sleep(randint(2, 4))
    driver.quit()
    print(len(html_srcs))

    for src in html_srcs:
        soup = BeautifulSoup(src, 'html.parser')

        # Find job elements
        job_elements = soup.find_all(class_='job_seen_beacon')

        # Extract job titles
        for job_element in job_elements:
            print("---------------")
            job_title = job_element.find(class_='jobTitle').text.strip()
            print(job_title)
            location = job_element.find(class_="company_location").text
            print(location)
            company_name = job_element.find(
                "span", {"data-testid": "company-name"}).text
            print(company_name)
            a = job_element.find(class_='jobTitle').find("a")
            print(a.get('href'))
            print(a.get("id"))


if __name__ == "__main__":
    main()
